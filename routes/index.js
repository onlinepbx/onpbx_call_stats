var express = require('express');
var router = express.Router();
// var db = require('../config/db.js');

/* GET home page. */
router.get('/', function(req, res, next) {
    // create tables if not already
    // db.sequelize.sync();

    res.render('index', { title: 'Статистика звонков' });
});

module.exports = router;

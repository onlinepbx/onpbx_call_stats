var _ = require('lodash');

var express = require('express');
var router = express.Router();
// var db = require('../config/db.js');

var amocrm = require('../amocrm_rest_lib.js');

// проверяем аутентификацию
router.post('/', function(req, res, next) {
    // create tables if not already
    // db.sequelize.sync();

    var amo_domain = req.body.amo_domain;
    var amo_email = req.body.amo_email;
    var amo_key = req.body.amo_key;

    var amoGroups = [{id: 0, name: 'Отдел продаж'}]; // группа по умолчанию
    var amoPersons = [];

    console.log(req.body);

    amocrm.auth(amo_domain, amo_email, amo_key, function(err, data) {
        console.log('err', err);
        console.log('res', data);

        if(err) return res.status(500).send(err.message);

        // db.Settings.findOrCreate({where: {amodomain: amo_domain}, defaults: {amologin: amo_email, amohash: amo_key, settings: {}}})
        // .spread(function(settingsObject, created) {
        //     if(settingsObject.hasOwnProperty('settings') &&
        //         settingsObject.settings.hasOwnProperty('users') && 
        //         settingsObject.settings.hasOwnProperty('groups')) {

        //         return res.json({
        //             groups: settingsObject.settigns.groups,
        //             users: settingsObject.settings.users,
        //             onpbx_domain: settingsObject.onpbxdomain,
        //             onpbx_key: settingsObject.onpbxkey,
        //             sys_nums: settingsObject.settings.sys_nums
        //         });
        //     }

            amocrm.query('GET', '/private/api/v2/json/accounts/current', {}, function(err, data) {
                if(err) return res.status(500).send(err.message);

                amoGroups = amoGroups.concat(data.account.groups);
                amoPersons = data.account.users.map(function(user) {
                    return {
                        id: user.id,
                        group_id: user.group_id,
                        group: _.get(_.find(amoGroups, {id: user.group_id}), 'name'),
                        name: _.compact([user.name, user.last_name]).join(' '),
                        number: ''
                    }
                });

                res.json({
                    groups: amoGroups,
                    users: amoPersons
                });
            });
        // })
        // .catch(function(err) {
        //     console.log(err.message, err.stack);
        //     res.status(500).send(err.message);
        // })
    });
});

module.exports = router;

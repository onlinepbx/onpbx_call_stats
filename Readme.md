## Установка

    :::sh
    git clone git@bitbucket.org:onlinepbx/onpbx_call_stats.git
    npm install
    bower install

## Запуск на 3000 порту

    :::sh
    node server.js
    # или 
    npm start

## Запуск на другом порту

    :::sh
    PORT=8080 node server.js

(вместо 8080 можно использовать любой свободный порт)

## Сохранение настроек

Настройки (API ключи, список сотрудников и т.п.) сохраняются в localStorage
браузера, а не на сервере.

window.onload = function() {
    $('#after-amo-login').hide();
    
    $('[name]').each(function() {
        if(localStorage.hasOwnProperty(this.name))
            this.value = localStorage[this.name];
    });


    document.getElementById('get-amo-info').onclick = function(e) {
        e.preventDefault();

        var request = {
            amo_domain: document.getElementById('amo_domain').value,
            amo_email: document.getElementById('amo_email').value,
            amo_key: document.getElementById('amo_key').value
        };

        $.post('/amo', request, function(response) {
            $('#before-amo-login').hide();
            $('#after-amo-login').show();

            var users_csv = 'Имя,Отдел,Внутренний номер\n';
            if(response.hasOwnProperty('users')) {
                response.users.forEach(function(user) {
                    users_csv += user.name + ',' + user.group + ',' + user.number + '\n';
                });
            }
            $('#users_csv').val(users_csv);

            console.log(response);
        });
    };

    document.getElementById('show-stats').onclick = function(e) {
        e.preventDefault();

        $('[name]').each(function() {
            localStorage[this.name] = this.value;
        });

        $('#stats-form').submit();
    };

    document.getElementById('clear-data').onclick = function(e) {
        e.preventDefault();

        localStorage.clear();
        $('[name]').each(function() {
            $(this).val("");
        });
    };
};
var _ = require('lodash');
var onpbx = require('../onpbx_rest_lib');

var express = require('express');
var router = express.Router();

function format_decimal(num, digits) {
    if(typeof digits !== 'number') digits = 2;
    var pow = Number('1e' + digits);
    return Math.round(num * pow) / pow;
}


/* GET stats listing. */
router.post('/', function(req, res, next) {
    var persons = [{
        name: 'Остальные',
        call_count: 0,
        call_percent: 0,
        auxiliary: true
    }, {
        name: 'Пропущено',
        call_count: 0,
        call_percent: 0,
        auxiliary: true
    }, {
        name: 'Сброс клиентом',
        call_count: 0,
        call_percent: 0,
        auxiliary: true
    }];
    var persons_by_number = {
        'other': persons[0],
        'missed': persons[1],
        'ivr': persons[2]
    };
    function Person(number, name, group) {
        persons_by_number[number] = this;
        this.number = number;
        this.name = name;
        this.group = group;
        this.call_count = 0;
        this.total_length = 0;
        this.call_percent = 0;
        this.auxiliary = false;
    }

    var calls_by_hour = {};

    var user_groups = [];

    var table3 = {
        'missed': 0,
        'ivr': 0,
        'total': 0
    };



    var domain = req.body.onpbx_domain;
    var key = req.body.onpbx_key;
    try {
        var date_from = new Date(req.body.start_date + ' 00:00:00 GMT+0500 (YEKT)').toUTCString().split(', ')[1];
        var date_to = new Date(req.body.end_date + ' 23:59:59 GMT+0500 (YEKT)').toUTCString().split(', ')[1];
    } catch(e) {
        console.log(e.stack);
        return res.status(500).send(e.message);
    }
    console.log(date_from, date_to);

    var sys_numbers = _.map(req.body.sys_nums.split(','), function(s) { return s.trim(); });
    sys_numbers.forEach(function(num) {
        persons_by_number[num] = persons[1];
    });
    persons[1].number = sys_numbers.join(', ');

    var ivr_numbers = _.map(req.body.ivr_nums.split(','), function(s) { return s.trim(); });
    ivr_numbers.forEach(function(num) {
        persons_by_number[num] = persons[2];
    });
    persons[2].number = ivr_numbers.join(', ');

    var type_filter = _.compact([
        req.body.show_incoming && 'inbound',
        req.body.show_outgoing && 'outbound',
        req.body.show_local && 'local',
        req.body.show_sip && 'sip',
    ]);

    var users_csv = req.body.users_csv.split('\n');
    _.forEach(users_csv, function(row) {
        row = row.split(',');
        var group = row[1].trim();
        persons.push(new Person(row[2].trim(), row[0].trim(), group));
        if(user_groups.indexOf(group) === -1) {
            user_groups.push(group);
        }
    });
    user_groups.forEach(function(group) {
        table3[group] = 0;
    });

    onpbx.performAuth(domain, key, function(err, data) {
        if(err) return res.redirect('/');

        onpbx.query('onlinepbx.ru', '/history/search.json', {
            date_from: date_from,
            date_to: date_to
        }, function(err, call_records) {
            if(err) return res.redirect('/');

            call_records = _.filter(call_records, function(call) {
                return type_filter.indexOf(call.type) !== -1;
            });

            var list_of_days = {};


            var counted_calls = 0;
            function processPerson(person, length) {
                if(!person.auxiliary) {
                    person.call_count += 1;
                    person.total_length += length;
                    counted_calls += 1;

                    if(person.group) {
                        table3[person.group] += 1;
                    }
                }
                else {
                    person.call_count += 1;
                    counted_calls += 1;

                    if(person === persons_by_number['ivr']) {
                        table3.ivr += 1;
                    }
                    if(person === persons_by_number['missed']) {
                        table3.missed += 1;
                    }
                }
            }
            _.forEach(call_records, function(call, i) {
                var number;
                if(req.body.show_incoming && call.type === 'inbound' && persons_by_number.hasOwnProperty(call.to)) {
                    processPerson(persons_by_number[call.to], Number(call.billsec));
                    number = call.to;
                }
                else if(req.body.show_outgoing && call.type === 'outbound' && persons_by_number.hasOwnProperty(call.caller)) {
                    processPerson(persons_by_number[call.caller], Number(call.billsec));
                    number = call.caller;
                }

                var dateObject = new Date(call.date * 1000);
                var hour = dateObject.getHours();
                hour = hour;
                var date = dateObject.toDateString();
                list_of_days[date] = 1;

                if(typeof calls_by_hour[hour] !== 'object') {
                    calls_by_hour[hour] = {};
                }
                if(typeof calls_by_hour[hour][date] !== 'number') {
                    calls_by_hour[hour][date] = 0;
                }
                if(persons_by_number[number] !== persons_by_number['ivr']) {
                    calls_by_hour[hour][date] += 1;
                }
            });

            var total_calls = call_records.length;
            var uncalls = persons_by_number['ivr'].call_count;
            var real_calls = total_calls - uncalls;
            counted_calls -= uncalls;
            persons_by_number['other'].call_count = total_calls - counted_calls;

            persons.forEach(function(person) {
                person.call_percent = Math.round(10000 * person.call_count / real_calls) / 100;
                if(!person.auxiliary) person.avg_length = Math.round(person.total_length / person.call_count / 6) / 10;
            });
            persons_by_number['ivr'].call_percent = Math.round(10000 * uncalls / total_calls) / 100;
            persons_by_number['other'].call_percent = Math.round(10000 * (total_calls - counted_calls) / total_calls) / 100;

            console.log('total', total_calls);
            console.log('uncalls', uncalls);
            console.log('real_calls', real_calls);
            console.log('counted_calls', counted_calls);

            // calls_by_hour = _.pairs(calls_by_hour);
            calls_by_hour1 = _.map(calls_by_hour, function(value, key) {
                return {
                    hour: key + '-' + (Number(key) + 1),
                    calls: _.mapValues(value, function(number) {
                        return { value: number, red: false };
                    })
                };
            });
            list_of_days = _.sortBy(_.keys(list_of_days), Date.parse);

            // помечаем красным
            list_of_days.forEach(function(day, dayId) {
                var max = _.max(calls_by_hour1, function(calls_by_hour) {
                    return _.get(calls_by_hour.calls[day], 'value');
                });
                max = max.calls[day].value;
                if(max) {
                    calls_by_hour1.forEach(function(calls_by_hour) {
                        if(calls_by_hour.hasOwnProperty('calls') && 
                            calls_by_hour.calls.hasOwnProperty(day) &&
                            calls_by_hour.calls[day].value == max)
                            calls_by_hour.calls[day].red = true;
                    });
                }
            });


            // таблица 4
            var table4 = _.mapValues(table3, function(value) {
                return format_decimal(100 * value / (counted_calls + uncalls), 2);
            });

            // таблица 5
            var table5 = _.mapValues(table3, function(value) {
                return format_decimal(100 * value / counted_calls, 2);
            });

            // таблица 3
            table3.total = counted_calls + uncalls;
            table5.total = counted_calls;

            res.render('stats', {
                title: 'Статистика звонков с ' + date_from + ' по ' + date_to, 
                start_date: date_from,
                end_date: date_to,
                days: list_of_days,
                groups: user_groups,
                persons: _.sortBy(persons, 'number'),
                calls_by_hour: calls_by_hour1,
                table3: table3,
                table4: table4,
                table5: table5
            }); 
        });
    });



});

module.exports = router;

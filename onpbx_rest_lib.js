/**
 * OnlinePBX HTTP API v1 node.js module
 * 
 * @see API docs: {@link http://api.onlinepbx.ru/}
 * @module onpbx_rest_lib
 * @author (C) 2015 onlinePBX inc.
 * @license Apache-2.0
 */

var http = require('http');
var https = require('https');
var CryptoJS = {
    'MD5': require('crypto-js/md5'),
    'HmacSHA1': require('crypto-js/hmac-sha1'),
    'base64': require('crypto-js/enc-base64')
};

var MAX_RETRIES = 5;

var keyStorage = {};

/**
 * Формирует тело POST-запроса в соответствии с требованиями сервера
 *
 * @private
 * @param {Object} formdata
 * @param {Number} [numeric_prefix]
 * @param {String} [arg_separator='&']
 * @returns {String}
 */
function httpBuildQuery(formdata, numeric_prefix, arg_separator) {
    var value, key, tmp = [];

    function _http_build_query_helper(key, val, arg_separator) {
        var k, tmp = [];
        if (val === true) {
            val = "1";
        }
        else if (val === false) {
            val = "0";
        }
        if (val != null) {
            if (typeof(val) === "object") {
                for (k in val) {
                    if (val[k] != null) {
                        tmp.push(_http_build_query_helper(key + "[" + k + "]", val[k], arg_separator));
                    }
                }
            }
            else {
                tmp.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
            }
            return tmp.join(arg_separator);
        }
        else {
            return '';
        }
    }

    if (!arg_separator) {
        arg_separator = "&";
    }

    for (key in formdata) {
        value = formdata[key];
        if (numeric_prefix && !isNaN(key)) {
            key = String(numeric_prefix) + key;
        }
        var query = _http_build_query_helper(key, value, arg_separator);
        if (query != '') {
            tmp.push(query);
        }
    }

    return tmp.join(arg_separator);
}

/**
 * Получает секретный ключ с сервера и сохраняет его в памяти (объект keyStorage)
 *
 * @alias module:onpbx_rest_lib.performAuth
 * @param {String} domain - домен (вида example.onpbx.ru)
 * @param {String} auth_key - API-ключ для аутентификации
 * @param {onpbxRequestApiKeyCallback} [cb] - callback-функция
 */
function onpbx_request_api_key(domain, auth_key, cb) {
    if(typeof cb !== 'function') cb = function(){};

    var postData = httpBuildQuery({
        auth_key: auth_key,
        new: true
    });
    var path = '/' + domain + '/auth.json';

    var options = {
        hostname: 'api.onlinepbx.ru',
        port: 80,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;',
            'Content-Length': postData.length
        }
    };

    var req = http.request(options, function(res) {
        res.on('data', function(chunk) {
            try {
                if(res.status < 200 || res.status >= 300) {
                    throw new Error('HTTP Error ' + res.status);
                }
                var data = JSON.parse(chunk);
                if(!data.status) {
                    throw new Error(data.comment);
                }

                // всё ОК, запоминаем ключ
                keyStorage[domain] = {
                    auth_key: auth_key,
                    key: data.data.key,
                    key_id: data.data.key_id
                };
                cb(undefined, data.data);
            } catch (e) {
                cb(e);
            }
        });
    });

    req.on('error', function(e) {
        cb(e);
    });

    req.write(postData);
    req.end();
}
/**
 * Вызывается в onpbx_request_api_key
 *
 * @callback onpbxRequestApiKeyCallback
 * @param {Error} error - ошибка, если возникла, или undefined
 * @param {Object} keyData
 * @param {String} keyData.key - секретная часть ключа
 * @param {String} keyData.key_id - публичный идентификатор ключа
 */

/**
 * Подписывает запрос. Вызывает onpbx_request_api_key, если в keyStorage нет секретного ключа для
 * указанного домена
 *
 * @private
 * @param {String} domain
 * @param {Array} request
 * @param {String} request.0 - метод
 * @param {String} request.1 - md5 сумма тела запроса
 * @param {String} request.2 - заголовок запроса Content-Type
 * @param {String} request.3 - заголовок запроса x-pbx-date (должен быть равен текущей дате)
 * @param {String} request.4 - URL запроса (вида api.onlinepbx.ru/example.onlinepbx.ru/somemethod.json)
 * @param {onpbxSignCallback} cb
 */
function onpbx_sign(domain, request, cb) {
    if(!keyStorage.hasOwnProperty(domain) || typeof keyStorage[domain] === 'undefined' ||
        !keyStorage[domain].hasOwnProperty('auth_key')) setImmediate(function() { cb(new Error('Provide auth_key first')); });

    var keyData = keyStorage[domain];
    var stringToSign = request.join('\n') + '\n';

    if(keyData.hasOwnProperty('key')) {
        setImmediate(function() {
            var signature = new Buffer(CryptoJS.HmacSHA1(stringToSign, keyData.key).toString()).toString('base64');
            cb(undefined, signature);
        });
    }
    else {
        onpbx_request_api_key(domain, keyData.auth_key, function(newKeyData) {
            var signature = new Buffer(CryptoJS.HmacSHA1(stringToSign, newKeyData.key).toString()).toString('base64');
            cb(undefined, signature);
        });
    }
}
/**
 * Вызывается функцией onpbx_sign
 *
 * @callback onpbxSignCallback
 * @param {Error} [err] - ошибка, если возникла
 * @param {String} [signature] - подпись, если ошибок не было
 */

/**
 * Совершает POST-запрос к серверу API
 *
 * @alias module:onpbx_rest_lib.query
 * @param {String} domain - домен (вида example.onpbx.ru)
 * @param {String} path - URL запроса (для запроса api.onlinepbx.ru/example.onlinepbx.ru/someobject/somemethod.json имеет вид /someobject/somemethod.json)
 * @param {Object} request - запрос в виде JS объекта
 * @param {onpbxPostCallback} cb
 */
function onpbx_post(domain, path, request, cb, retry) {
    if(typeof cb !== 'function') cb = function() {};
    if(typeof retry === 'undefined') retry = 0;

    var keyData = keyStorage[domain];

    var contentType = 'application/x-www-form-urlencoded; charset=UTF-8;';
    var postData = httpBuildQuery(request);
    var postMD5 = CryptoJS.MD5(postData).toString();
    var date = new Date();

    // получаем подпись
    var signature = onpbx_sign(domain, [
        'POST',
        postMD5,
        contentType,
        date.toString(),
        'api.onlinepbx.ru/' + domain + path
    ], function(err, signature) {
        if(err) return cb(err);

        var options = {
            hostname: 'api.onlinepbx.ru',
            port: 80,
            path: '/' + domain + path,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': contentType,
                'Content-MD5': postMD5,
                'Content-Length': postData.length,
                'x-pbx-date': date.toString(),
                'x-pbx-authentication': keyData.key_id + ':' + signature,
            }
        };

        var req = http.request(options, function(res) {
            var jsonstr = "";
            res.on('data', function(chunk) {
                jsonstr += chunk;
            });
            res.on('end', function() {
                var data = JSON.parse(jsonstr);

                // нужно получить новый секретный ключ и попробовать снова
                if(data.status == 0 && data.comment === 'not authenticated') {
                    if(retry < MAX_RETRIES) {
                        return onpbx_request_api_key(domain, keyData.auth_key, function(err, res) {
                            // не смогли авторизоваться, (auth_key изменился и больше не подходит?)
                            if(err) return cb(err);
                            // смогли получить новый ключ, пробуем снова
                            onpbx_post(domain, path, request, cb, retry + 1);
                        });
                    }
                    else {
                        console.log(data, data.status, data.comment, retry);
                        // по MAX_RETRIES-ному разу не получилось, что-то явно не так
                        return cb(new Error('Could not authenticate'));
                    }
                }

                // какая-то другая ошибка, возвращаем её
                if(!data.status) {
                    return cb(new Error(data.comment));
                }

                // всё в норме, возвращаем результат
                cb(undefined, data.data);
            });
        });

        req.on('error', function(err) {
            cb(err);
        });

        req.write(postData);
        req.end();
    });
}
/**
 * Вызывается функцией onpbx_post
 *
 * @callback onpbxPostCallback
 * @param {Error} [err] - ошибка, если возникла или поле status ответа 0 (поле comment в err.message)
 * @param {Object} [res] - JS-объект с данными, которые вернул сервер
 */

/**
 * Обновляет API ключ (удаляет старую аутентификацию при необходимости)
 *
 * @param {String} domain - домен (вида example.onpbx.ru)
 * @param {String} auth_key - API-ключ для аутентификации (из панели)
 */
exports.setApiKey = function(domain, auth_key) {
    if(!keyStorage.hasOwnProperty(domain) ||
        !keyStorage[domain].hasOwnProperty(auth_key) ||
        keyStorage[domain][auth_key] !== auth_key) {
        keyStorage[domain] = {
            auth_key: api_key
        };
    }
}

exports.performAuth = onpbx_request_api_key;
exports.query = onpbx_post;

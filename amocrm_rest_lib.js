var https = require('https');
var querystring = require('querystring');

function httpBuildQuery(formdata, numeric_prefix, arg_separator) {
    var value, key, tmp = [];

    function _http_build_query_helper(key, val, arg_separator) {
        var k, tmp = [];
        if (val === true) {
            val = "1";
        }
        else if (val === false) {
            val = "0";
        }
        if (val != null) {
            if (typeof(val) === "object") {
                for (k in val) {
                    if (val[k] != null) {
                        tmp.push(_http_build_query_helper(key + "[" + k + "]", val[k], arg_separator));
                    }
                }
            }
            else {
                tmp.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
            }
            return tmp.join(arg_separator);
        }
        else {
            return '';
        }
    }

    if (!arg_separator) {
        arg_separator = "&";
    }

    for (key in formdata) {
        value = formdata[key];
        if (numeric_prefix && !isNaN(key)) {
            key = String(numeric_prefix) + key;
        }
        var query = _http_build_query_helper(key, value, arg_separator);
        if (query != '') {
            tmp.push(query);
        }
    }

    return tmp.join(arg_separator);
}

var authData = {};

exports.auth = function(domain, login, hash, cb) {
    var postData = JSON.stringify({
        "USER_LOGIN": login,
        "USER_HASH": hash
    });
    
    var options = {
        hostname: domain,
        port: 443,
        path: '/private/api/auth.php?type=json',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;',
            'Content-Length': postData.length
        }
    };

    var req = https.request(options, function(res) {
        fullResponse = '';
        res.on('data', function(chunk) {
            fullResponse += chunk;
        });
        res.on('end', function() {
            try {
                var data = JSON.parse(fullResponse).response;

                if(data.auth) {
                    var amoCookies = '';
                    res.headers['set-cookie'].forEach(function(cookie) {
                        amoCookies += cookie.split(';')[0] + '; ';
                    });

                    authData = {
                        domain: domain,
                        cookies: amoCookies
                    };

                    cb(undefined, data);
                }
                else 
                    cb(new Error('amoCRM auth error: ' + data.error));
            } catch (e) {
                cb(e);
            }
        });
    });

    req.on('error', function(e) {
        cb(e);
    });

    req.write(postData);
    req.end();
}

exports.query = function(method, path, query, cb) {
    var options = {
        hostname: authData.domain,
        port: 443,
        path: path,
        method: method,
        headers: {
            'Cookie': authData.cookies
        }
    };

    if(method === 'POST') {
        var postData = JSON.stringify(query);
        options.headers['Content-Type'] = 'application/json';
        options.headers['Content-Length'] = postData.length;
    }

    var req = https.request(options, function(res) {
        fullResponse = '';
        res.on('data', function(chunk) {
            fullResponse += chunk;
        });
        res.on('end', function() {
            // console.log('got data ->', fullResponse);
            try {
                var data = JSON.parse(fullResponse).response;

                cb(undefined, data);
            } catch (e) {
                cb(e);
            }
        });
    });

    // console.log('sending', options, postData);

    req.on('error', function(e) {
        cb(e);
    })

    if(method === 'POST') req.write(postData);
    req.end();
}
